<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Order::class, 15)
            ->create([
                         'buyer_id' => function () {
                             return factory(\App\Buyer::class, 1)->create()->first()->id;
                         }
                     ])
            ->each(function ($order) {
                /**
                 * @var \App\Order $order
                 */
                $order->orderItems()->saveMany(
                    factory(\App\OrderItem::class, 10)
                        ->make(['order_id' => $order->all()->shuffle()->first()->id])
                );
            });
    }
}
