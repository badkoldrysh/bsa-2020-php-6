<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $quantity = $faker->numberBetween(1, 10);
    $price = $faker->numberBetween(100, 10000);
    $discount = (float) ($faker->numberBetween(0, 100) / 100);
    $sum = $quantity * $price * $discount;
    return [
        'quantity' => $quantity,
        'price' => $price,
        'discount' => $discount,
        'sum' => $sum,
        'product_id' => function () {
                            return \App\Product::all()->shuffle()->first()->id;
        }
    ];
});
