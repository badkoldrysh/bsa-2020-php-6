<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductResource;
use App\Order;
use App\OrderItem;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\Console\Input\Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();

        return $orders->map(
            function ($order) {
                return new OrderResource($order);
            }
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $order = Order::create(
            [
                'date' => Carbon::now(),
                'buyer_id' => $data['buyerId']
            ]
        );

        foreach ($data['orderItems'] as $prodData) {
            $product = Product::find($prodData['productId']);

            $itemsData = [
                'quantity' => $prodData['productQty'],
                'discount' => $prodData['productDiscount'],
                'price' => $product->price,
                'sum' => $prodData['productQty'] * $prodData['productDiscount'] * $product->price,
                'product_id' => $product->id,
                'order_id' => $order->id,
            ];
            OrderItem::create($itemsData);
        }

        return new Response(new OrderResource($order));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        if ($order === null) {
            return response("Order with id=$id doesn't exists", 404);
        }

        return new OrderResource(Order::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        if (!$order) {
            return new Response(
                [
                    'result' => 'fail',
                    'message' => 'product not found'
                ]
            );
        }

        $order->orderItems()->delete();

        $data = $request->all();

        foreach ($data['orderItems'] as $prodData) {
            $product = Product::find($prodData['productId']);

            $itemsData = [
                'quantity' => $prodData['productQty'],
                'discount' => $prodData['productDiscount'],
                'price' => $product->price,
                'sum' => $prodData['productQty'] * $prodData['productDiscount'] * $product->price,
                'product_id' => $product->id,
                'order_id' => $order->id,
            ];
            OrderItem::create($itemsData);
        }

        return new Response(new OrderResource($order));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Order::destroy($id);

        return ['result' => $result ? 'success' : 'failed'];
    }
}
