<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $orderItems = $this->orderItems;

        $orderSum = 0;
        foreach ($orderItems as $item) {
            $orderSum += $item->sum;
        }

        $buyer = $this->buyer;
        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $orderSum,
            'orderItems' => $orderItems,
            'buyer' => [
                'buyerFullName' => $buyer->name . ' ' . $buyer->surname,
                'buyerAddress' => $buyer->country . ' ' . $buyer->city . ' ' . $buyer->addressLine,
                'buyerPhone' => $buyer->phone,
            ],
        ];
    }
}
